# Copyright 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A generic C++ template library for sparse, dense and skyline matrices"
DESCRIPTION="
Gmm++ is a generic C++ template library for sparse, dense and skyline matrices.
It is built as a set of generic algorithms (mult, add, copy, sub-matrices,
dense and sparse solvers ...) for any interfaced vector type or matrix type. It
can be view as a glue library allowing cooperation between several vector and
matrix types. However, basic sparse, dense and skyline matrix/vector types are
built in Gmm++, hence it can be used as a standalone linear algebra library.
Interfacing a vector or matrix type means writing 'traits' objects called
'linalg_traits', which describe their properties. The library offers predefined
dense, sparse and skyline matrix types.
"
HOMEPAGE="http://getfem.org/gmm/index.html"
DOWNLOADS="https://download-mirror.savannah.gnu.org/releases/getfem/stable/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

